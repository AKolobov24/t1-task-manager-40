package ru.t1.akolobov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    public static final String DESCRIPTION = "Load backup data from base64 file.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getDomainEndpoint().loadBackup(new DataBackupLoadRequest(getToken()));
    }

}
