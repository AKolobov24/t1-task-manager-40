package ru.t1.akolobov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.akolobov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class ProjectDisplayByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-display-by-id";

    @NotNull
    public static final String DESCRIPTION = "Find project by Id and display.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getById(request);
        @Nullable Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
        displayProject(project);
    }

}
