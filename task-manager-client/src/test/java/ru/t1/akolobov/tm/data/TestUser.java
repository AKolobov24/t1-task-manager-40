package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.User;

public final class TestUser {

    @NotNull
    public static final User NEW_USER = createUser();

    @NotNull
    private static User createUser() {
        User user = new User();
        user.setLogin("NEW_USER");
        user.setEmail("new_user@email.com");
        return user;
    }

}
