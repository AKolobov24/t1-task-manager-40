package ru.t1.akolobov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private String email;

    @NotNull
    private Role role = Role.USUAL;

    private boolean locked = false;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof User) {
            User anotherUser = (User) obj;
            return this.getId().equals(anotherUser.getId()) &&
                    this.getLogin().equals(anotherUser.getLogin());
        }
        return false;
    }

}
