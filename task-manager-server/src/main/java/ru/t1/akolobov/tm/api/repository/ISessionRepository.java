package ru.t1.akolobov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Update("CREATE TABLE IF NOT EXISTS tm_session " +
            "( " +
            "id character(36) PRIMARY KEY," +
            "created timestamp," +
            "user_id character(36) REFERENCES tm_user (id)," +
            "date timestamp," +
            "role varchar(255)" +
            ");"
    )
    void checkDatabaseTable();

    @Insert("INSERT INTO tm_session " +
            "(id, created, user_id, date, role) " +
            "VALUES(#{id}, #{created}, #{userId}, #{date}, #{role})"
    )
    void add(@NotNull Session model);

    @Update("UPDATE tm_session " +
            "SET date = #{date}, " +
            "role = #{role}, " +
            "user_id = #{userId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull Session model);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_session")
    void clearAll();

    @Select("SELECT count(*) > 0 FROM tm_session " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1"
    )
    boolean existById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<Session> findAll();

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<Session> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_session " +
            "WHERE user_id = #{userId} " +
            "ORDER BY #{sortColumn}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<Session> findAllSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sortColumn") String sortColumn
    );

    @Select("SELECT * FROM tm_session " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable Session findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT count(*) FROM tm_session WHERE user_id = #{userId}")
    @NotNull Integer getSize(@NotNull String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull Session model);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id);

}
