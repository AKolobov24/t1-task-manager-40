package ru.t1.akolobov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Update(
            "CREATE TABLE IF NOT EXISTS tm_user " +
                    "( " +
                    "id character(36) PRIMARY KEY," +
                    "created timestamp," +
                    "login varchar(255)," +
                    "password_hash varchar(255)," +
                    "role varchar(255)," +
                    "locked boolean," +
                    "email varchar(255)," +
                    "first_name varchar(255)," +
                    "last_name varchar(255)," +
                    "middle_name varchar(255)" +
                    ")"
    )
    void checkDatabaseTable();

    @Update(
            "INSERT INTO tm_user (" +
                    "id, " +
                    "created, " +
                    "login, " +
                    "password_hash, " +
                    "role, " +
                    "locked, " +
                    "email, " +
                    "first_name, " +
                    "last_name, " +
                    "middle_name" +
                    ") "
                    + "VALUES(" +
                    "#{id}, " +
                    "#{created}, " +
                    "#{login}, " +
                    "#{passwordHash}, " +
                    "#{role}, " +
                    "#{locked}, " +
                    "#{email}, " +
                    "#{firstName}, " +
                    "#{lastName}, " +
                    "#{middleName}" +
                    ")"
    )
    void add(@NotNull User model);

    @Update("UPDATE tm_user " +
            "SET login = #{login}, " +
            "password_hash = #{passwordHash}, " +
            "role = #{role}, " +
            "locked = #{locked}, " +
            "email = #{email}, " +
            "first_name = #{firstName}, " +
            "last_name = #{lastName}, " +
            "middle_name = #{middleName} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull User model);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT count(*) > 0 FROM tm_user " +
            "WHERE id = #{id} LIMIT 1"
    )
    boolean existById(@NotNull String id);

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @NotNull List<User> findAll();

    @Select("SELECT * FROM tm_user " +
            "ORDER BY #{sortColumn}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @NotNull List<User> findAllSorted(@NotNull String sortColumn);

    @Select("SELECT * FROM tm_user " +
            "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable User findOneById(@NotNull String id);

    @Select("SELECT count(*) FROM tm_user")
    @NotNull Integer getSize();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull User model);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@NotNull String id);

    @Select("SELECT * FROM tm_user " +
            "WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable User findByLogin(@NotNull String login);

    @Select("SELECT * FROM tm_user " +
            "WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable User findByEmail(@NotNull String email);

    @Select("SELECT count(*) > 0 FROM tm_user " +
            "WHERE login = #{login} LIMIT 1")
    boolean isLoginExist(@NotNull String login);

    @Select("SELECT count(*) > 0 FROM tm_user " +
            "WHERE email = #{email} LIMIT 1")
    boolean isEmailExist(@NotNull String email);

}
