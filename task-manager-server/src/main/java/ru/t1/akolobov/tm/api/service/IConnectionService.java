package ru.t1.akolobov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull
    SqlSession getConnection();

    @NotNull
    SqlSession getConnection(boolean autoCommit);

}
