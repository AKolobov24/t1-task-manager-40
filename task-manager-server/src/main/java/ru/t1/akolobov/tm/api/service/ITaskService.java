package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull Task add(@Nullable Task task);

    @NotNull
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    Task update(@Nullable String userId, @Nullable Task task);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Integer getSize(@Nullable String userId);

    @NotNull
    Task remove(@Nullable String userId, @Nullable Task task);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, Status status);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> models);

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> models);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
