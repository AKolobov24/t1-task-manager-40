package ru.t1.akolobov.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final DataSource dataSource = new PooledDataSource(
                databaseProperty.getDatabaseDriver(),
                databaseProperty.getDatabaseUrl(),
                databaseProperty.getDatabaseUser(),
                databaseProperty.getDatabasePassword()
        );
        @NotNull final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        @NotNull final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public SqlSession getConnection() {
        return getConnection(false);
    }

    @NotNull
    @Override
    public SqlSession getConnection(boolean autoCommit) {
        return sqlSessionFactory.openSession(autoCommit);
    }

}
