package ru.t1.akolobov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.NameEmptyException;
import ru.t1.akolobov.tm.exception.field.StatusEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.model.Project;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class ProjectService extends AbstractService implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new EntityNotFoundException();
        project.setUserId(userId);
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            repository.add(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project update(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new EntityNotFoundException();
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            if (!repository.existById(userId, project.getId()))
                throw new ProjectNotFoundException();
            repository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            repository.clearAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.existById(userId, id);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(final @Nullable String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.findAllByUserIdSorted(userId, sort.getColumnName());
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.findOneById(userId, id);
        }
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            return repository.getSizeByUserId(userId);
        }
    }

    @NotNull
    @Override
    public Project remove(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new EntityNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            if (!repository.existById(userId, project.getId()))
                throw new ProjectNotFoundException();
            repository.remove(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            repository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return add(userId, new Project(name));
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return add(userId, new Project(name));
        return add(userId, new Project(name, description));
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return add(userId, new Project(name));
        return add(userId, new Project(name, status));
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @NotNull
    public Collection<Project> add(@NotNull Collection<Project> models) {
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            models.forEach(repository::add);
            connection.commit();
            return models;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @NotNull
    public Collection<Project> set(@NotNull Collection<Project> models) {
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository =
                    connection.getMapper(IProjectRepository.class);
            repository.clearAll();
            models.forEach(repository::add);
            connection.commit();
            return models;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}