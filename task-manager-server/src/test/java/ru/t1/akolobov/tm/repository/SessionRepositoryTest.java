package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestSession.createSession;
import static ru.t1.akolobov.tm.data.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    private static SqlSession connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void clearData() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull Session session = createSession(USER1_ID);
        repository.add(session);
        Assert.assertEquals(session, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        List<Session> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::add);
        Session user2Session = createSession(USER2_ID);
        repository.add(user2Session);
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        repository.clear(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        @NotNull Session session = createSession(USER1_ID);
        repository.add(session);
        Assert.assertTrue(repository.existById(USER1_ID, session.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        List<Session> user1SessionList = createSessionList(USER1_ID);
        List<Session> user2SessionList = createSessionList(USER2_ID);
        user1SessionList.forEach(repository::add);
        user2SessionList.forEach(repository::add);
        Assert.assertEquals(user1SessionList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        @NotNull Session session = createSession(USER1_ID);
        repository.add(session);
        @NotNull String sessionId = session.getId();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, sessionId));
        Assert.assertNull(repository.findOneById(USER2_ID, sessionId));
    }

    @Test
    public void getSize() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        List<Session> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::add);
        Assert.assertEquals((Integer) sessionList.size(), repository.getSize(USER1_ID));
        repository.add(createSession(USER1_ID));
        Assert.assertEquals((Integer) (sessionList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        createSessionList(USER1_ID).forEach(repository::add);
        @NotNull Session session = createSession(USER1_ID);
        repository.add(session);
        Assert.assertEquals(session, repository.findOneById(USER1_ID, session.getId()));
        repository.remove(session);
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

    @Test
    public void removeById() {
        @NotNull ISessionRepository repository = connection.getMapper(ISessionRepository.class);
        createSessionList(USER1_ID).forEach(repository::add);
        @NotNull Session session = createSession(USER1_ID);
        repository.add(session);
        repository.removeById(USER1_ID, session.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

}
