package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    private static SqlSession connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
    }

    @AfterClass
    public static void closeConnection() {
        connection.close();
    }

    @Test
    public void add() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(USER1);
        Assert.assertTrue(repository.findAll().contains(USER1));
        repository.remove(USER1);
    }

    @Test
    @Ignore
    public void clear() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        List<User> userList = createUserList();
        userList.forEach(repository::add);
        Assert.assertEquals(userList.size(), repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existById() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(USER1);
        Assert.assertTrue(repository.existById(USER1.getId()));
        Assert.assertFalse(repository.existById(USER2.getId()));
        repository.remove(USER1);
    }

    @Test
    public void findAll() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        List<User> user1UserList = createUserList();
        user1UserList.forEach(repository::add);
        Assert.assertTrue(repository.findAll().containsAll(user1UserList));
        user1UserList.forEach(repository::remove);
    }

    @Test
    public void findOneById() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId()));
        repository.remove(USER1);
    }

    @Test
    public void getSize() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        List<User> userList = repository.findAll();
        Assert.assertEquals(userList.size(), repository.getSize().intValue());
        repository.add(NEW_USER);
        Assert.assertEquals((userList.size() + 1), repository.getSize().intValue());
        repository.remove(NEW_USER);
    }

    @Test
    public void remove() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findOneById(NEW_USER.getId()));
        Integer size = repository.getSize();
        repository.remove(NEW_USER);
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
    }

    @Test
    public void removeById() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(NEW_USER);
        Assert.assertNotNull(repository.findOneById(NEW_USER.getId()));
        repository.removeById(NEW_USER.getId());
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
    }

    @Test
    public void findByLogin() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
        Assert.assertNull(repository.findOneById(USER2.getLogin()));
        repository.remove(USER1);
    }

    @Test
    public void findByEmail() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
        Assert.assertNull(repository.findByEmail("user_2@email.ru"));
        repository.remove(USER1);
    }

    @Test
    public void isLoginExist() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        repository.add(USER1);
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        repository.remove(USER1);
    }

    @Test
    public void isEmailExist() {
        @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist("user_2@email.ru"));
        repository.remove(USER1);
    }

}
